import React from 'react';
import Menu from './Menu';
import DataGrid from './DataGrid';

class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {products: [], isLoading: false, filter: null};
    }

    componentDidMount() {
        this.setState({isLoading: true});
        this.fetchData();
    }

    fetchData(){
        fetch('/api/Auction')
            .then(response => response.json())
            .then(data => this.setState({products: data, isLoading: false}));
    }

    changeHandler(filter) {
        this.setState({filter: filter});
    }

    handleClick() {
        this.setState({filter: null});
    }

    onCountdownEnd () {
        this.fetchData();
    }

    render() {
        const isLoading = this.state.isLoading;
        if (isLoading) {
            return <p>Loading...</p>;
        }
        const categories = [...new Set(this.state.products.map(product => product.productCategory))];

        return (
            <div>
                <Menu categories={categories}
                      onClick={() => this.handleClick()}
                      onChange={this.changeHandler.bind(this)}/>
                <DataGrid products={this.state.products}
                          filter={this.state.filter}
                          onCountdownEnd={() => this.onCountdownEnd()}/>
            </div>
        );
    }
}

export default ProductList;