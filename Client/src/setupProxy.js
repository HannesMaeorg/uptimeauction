const proxy = require('http-proxy-middleware');

module.exports = function (app) {
    app.use(proxy('/api', {
        target: 'http://uptime-auction-api.azurewebsites.net/',
        changeOrigin: true
    }),
        proxy('/bids', {
            target: 'http://localhost:8080/',
            changeOrigin: true
        })
    );
};