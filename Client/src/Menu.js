import React from "react";

class Menu extends React.Component {

    render() {
        return (
            <div className="filter-menu">
                <span>
                <select onChange={() => this.props.onChange(this.menu.value)} ref={(input) => this.menu = input}>
                    {this.props.categories.map(category =>
                        <option key={category} value={category}>
                            {category}
                        </option>
                    )}
                </select>
                <button onClick={() => this.props.onClick()}>reset filter</button>
                </span>
            </div>
        );
    }
}

export default Menu;