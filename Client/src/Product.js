import React from "react";
import Moment from "moment";
import ReactMomentCountDown from "react-moment-countdown";
import Modal from "./Modal";


class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {show: false, visible: true};
    }

    showModal = () => {
        this.setState({show: true});
    };

    hideModal = () => {
        this.setState({show: false});
    };

    handleSubmit(event) {
        event.preventDefault();
        const formData = new FormData(event.target);;
        formData.append('productId', this.props.product.productId);
        let jsonObject = {};
        for (const [key, value]  of formData.entries()) {
            jsonObject[key] = value;
        }
        fetch('/bids', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'},
            body: JSON.stringify(jsonObject)
        });
        event.target.reset();
    }

    onCountdownEnd(){
        this.setState({visible: false})
        this.props.onCountdownEnd();
    }

    render() {
        const endDate = Moment(this.props.product.biddingEndDate);
        const taskClassName = this.state.visible ? '' : 'hidden';
        return (
            <tr className={taskClassName}>
                <td>{this.props.product.productName}</td>
                <td>{this.props.product.productDescription}</td>
                <td>{this.props.product.productCategory}</td>
                <td><ReactMomentCountDown  toDate={endDate} onCountdownEnd={() => this.onCountdownEnd()}/></td>
                <td className="button-container">
                    <button onClick={this.showModal}>bid</button>
                    <Modal show={this.state.show} handleClose={this.hideModal} >
                        <form onSubmit={this.handleSubmit.bind(this)} className="bidText">
                            <h3>First name</h3>
                            <input type="text" name="firstName" placeholder="first name.."/>
                            <h3> Last name </h3>
                            <input type="text" name="lastName" placeholder="last name.."/>
                            <h3> Sum </h3>
                            <input type="text" name="bidSum" placeholder="bid sum.."/>
                            <button onClick={this.hideModal}>make a bid</button>
                        </form>
                    </Modal>
                </td>
            </tr>
        )
    }
}

export default Product;