import React, { Component } from 'react'
import './App.css'
import ProductList from './ProductList';

class App extends Component {
    constructor () {
        super()
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick () {
        fetch('/api/Auction').then(response => console.log(response))
    }

    render () {
        return (
            <div className="App">
                <ProductList/>
            </div>
        )
    }
}

export default App

