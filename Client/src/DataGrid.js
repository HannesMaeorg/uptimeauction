import React from "react";
import Product from "./Product";

class DataGrid extends React.Component {
    render() {
        let products = this.props.products
        if (this.props.filter !== null) {
            products = products.filter(product => product.productCategory === this.props.filter);
        }

        const filteredProducts = products.map(product =>
            <Product key={product.productId} product={product} onCountdownEnd={() => this.props.onCountdownEnd()}/>
        );

        return (
            <div>
                <table>
                    <tbody>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Category</th>
                        <th>Auction end</th>
                        <th>Make a bid</th>
                    </tr>
                    {filteredProducts}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default DataGrid;