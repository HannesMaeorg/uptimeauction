package auction;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BidControllerTest {

    @Autowired
    private BidController controller;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    public void postRequestWorking() {

        Bid bid = new Bid("Laine",
                "Siinus",
                new BigDecimal(1111),
                "7f3a71f5-2fd5-4afc-9484-7220b988b496");

        assertThat(this.restTemplate.postForObject("http://localhost:" + port + "/bids", bid,
                Bid.class)).hasFieldOrProperty("lastName");
    }

    @Test
    public void postAndGetworking() throws Exception {

        post(new Bid("Alo G.",
                "Rütm",
                new BigDecimal(2.2),
                "7f3a71f5-2fd5-4afc-9484-7220b988b496"));
        Assertions.assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/bids",
                String.class)).contains("Alo G.");
    }

    private void post(Bid bid) {
        this.restTemplate.postForObject("http://localhost:" + port + "/bids", bid, Bid.class);

    }

}