package auction;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BidService {

    // Äriloogika

    private final BidRepository repository;

    public BidService(BidRepository repository) {
        this.repository = repository;
    }

    List<Bid> getAll() {
        return repository.findAll();
    }

    Bid saveBid(Bid newBid) {
        return repository.save(newBid);
    }
}
