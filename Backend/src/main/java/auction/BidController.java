package auction;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
class BidController {

    private final BidService service;
    private final BidRepository repository;

    BidController(BidService service, BidRepository repository) {
        this.service = service;
        this.repository = repository;
    }

    @GetMapping("/bids")
    List<Bid> all() {
        return service.getAll();
    }

    @PostMapping("/bids")
    Bid newBid(@RequestBody Bid newBid) {
        return service.saveBid(newBid);
    }
}
