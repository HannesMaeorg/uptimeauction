package auction;

import org.springframework.data.jpa.repository.JpaRepository;

interface BidRepository extends JpaRepository<Bid, Long> {

}
