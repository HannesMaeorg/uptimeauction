package auction;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
class Bid {
    private @Id
    @GeneratedValue
    Long id;
    private String firstName;
    private String lastName;
    private BigDecimal bidSum;
    private LocalDateTime bidTime = LocalDateTime.now();
    private String productId;

    Bid(String firstName, String lastName, BigDecimal bidSum, String productId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.bidSum = bidSum;
        this.productId = productId;
    }

}
